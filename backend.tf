terraform {
  backend "gcs" {
    bucket  = "tf-state-poc-neomed"
    prefix  = "terraform/state"
  }
}