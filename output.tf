output "google_project" {
  value = var.google_project
}
output "region" {
  value = var.region
}
output "cluster_name" {
  value = var.cluster_name
}

output "machine_type" {
  value = var.machine_type
}

output "disk_type" {
    value = var.disk_type
}

output "disk_size_gb" {
    value = var.disk_size_gb
}
output "endpoint" {
  value = "https://${google_container_cluster.octopus.endpoint}"
}
output "client_key" {
  value = base64decode(google_container_cluster.octopus.master_auth[0].client_key)
  sensitive = true
}
output "client_certificate" {
  value = base64decode(google_container_cluster.octopus.master_auth[0].client_certificate)
}
output "cluster_ca_certificate" {
  value = base64decode(google_container_cluster.octopus.master_auth[0].cluster_ca_certificate)
}
