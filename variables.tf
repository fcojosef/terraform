#cluster kubernetes
variable "google_project" {
  type = string
  description = "Nome do projeto no Google Cloud Platform"
  default  = "neon-radius-345202"
}

variable "region" {
  type = string
  description = "Região escolhida para abrigar os recursos de infraestrutura da Neomed"
  default = "us-central1"
}

variable "zone" {
  type = list(string)
  description = "lista com as zonas da região  us-central1"
  default = [ "us-central1-a", "us-central1-c" ]
}

variable "cluster_name" {
  type = string
  description = "Nome do cluster para o Octopus"
  default = "neomed-octopus"
}

variable "kubernetes_version" {
  type = string
  description = "Versão do Kubernetes (mais atual) do canal regular utilizado no cluster octopus"
  default = "1.21.6-gke.1503"
}

variable "gke_num_nodes" {
  type = number
  description = " Número de Nós. Deve-se considerar que os pools de nós serão criados multi-zona, assim um nó será criado para cada zona do cluster"
  default     = 1 
}

variable "neomed-vpc-octopus" {
  type = string
  description = "nome da rede utilizada no cluster octopus"
  default = "neomed-vpc-octopus"
}

variable "neomed-subnet-octopus" {
  type = string
  description = "nome da sub-rede utilizada no cluster octopus"
  default = "neomed-subnet-octopus"
}

variable "machine_type" {
   type = string
   description = "tipo de machine"
   default = "e2-small"
}
 
variable "disk_type" {
   type = string
   description = " o tipo de disco"
   default = "pd-ssd"
}

variable "disk_size_gb" {
   type = number
   description = " o tamanho do disco"
   default = 50
}

variable "terraform-service-account" {
  type = string
  description = "conta de serviço impessoal do terraform utilizada para gerenciar o cluster"
  #default = "1054135544511-compute@developer.gserviceaccount.com"
  default = "terraform@neon-radius-345202.iam.gserviceaccount.com"
}

variable "key-gitlab-runner" {
    type = string
    description = "chave para criar os runner com o projeto octopus"
    default = "GR1348941qioExM2xWxd3yh_KNm55"
    #default = "K4FXHRVy28cUC1xwsdeB"
  
}

variable "gitlab-chart-version" {
    type = string
    description = "Definir a versão do gitlab-runner"
    default = "0.40.0"
}

variable "gitlab-release-name" {
    type = string
    description = "Definir nome padrão para o gitlab-runner"
    default = "gitlab-runner"
}

variable "gitlab-repository" {
    type = string
    description = "Instalar o repositório padrão do  gitlab-runner"
    default = "https://charts.gitlab.io" 
  
}

variable "gitlab-chart" {
    type = string
    description = "O nome do repositório gitlab-runner"
    default = "gitlab-runner" 
  
}

variable "gitlab-namespace" {
    type = string
    description = "Definir o namespace padrão para o gitlab-runner"
    default = "gitlab-runners"
  
}

variable "concurrent-runner" {
    type = number
    description = "Definir a quantidade de runner concorrentes"
    default = 15
}

variable "check-interval" {
    type = number
    description = "Definir o intervalo de verificação dos runner"
    default = 2
}

#helm Ingress-controller nginx
variable "nginx-release-name" {
    type = string
    description = "Definir nome da versão do nginx controller"
    default = "ingress-nginx"
}

variable "nginx-ingress-namespace" {
    type = string
    description = "Definir nome do namespace padrao para ingress controller "
    default = "nginx-ingress"
}

variable "nginx-ingress-repository" {
    type = string
    description = "Definir o repositorio do chart ingress controller"
    default = "https://kubernetes.github.io/ingress-nginx"
}
variable "nginx-chart-version" {
    type = string
    description = "Definir a versão do nginx-controler"
    default = "4.0.10"
}
variable "number-replicas-nginx-controller" {
    type = number
    description = "Quantidade mínima de replicas iniciais"
    default = 2
}
#helm cert-manager
variable "release-cert-manager" {
    type = string
    description = "Definir nome da versão do cert-manager"
    default = "cert-manager"
}
variable "namespace-cert-manager" {
    type = string
    description = "Definir nome do namespace padrao para cert-manager"
    default = "cert-manager"
}
variable "cert-manager-repo" {
    type = string
    description = "Definir o repositorio do chart ingress controller"
    default = "https://charts.jetstack.io"
}

variable "cert-manager-version" {
    type = string
    description = "Definir a versão do cert-manager"
    default = "v1.6.1"
}

variable "server-letsencrypt" {
    type = string
    description = "Servidor que irá gerar o certificado"
    default = "https://acme-v02.api.letsencrypt.org/directory"
  
}
variable "email-issuer" {
    type = string
    description = "Definir o e-mail de registro no letsencrypt"
    default = "fcojosef.o@gmail.com"
}
variable "private-key-secret-ref" {
    type = string
    description = "Chave a ser utilizada para gerar os certificados"
    default = "letsencrypt-prod"
}