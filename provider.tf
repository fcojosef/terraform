data "google_client_config" "provider" {}
provider "helm" {
  kubernetes {
    host                   = "https://${google_container_cluster.octopus.endpoint}"
    cluster_ca_certificate = base64decode(google_container_cluster.octopus.master_auth[0].cluster_ca_certificate)
    token                  = data.google_client_config.provider.access_token
    }
}

provider "google" {
  credentials = "credentials-new.json"
  project     = var.google_project
  region      = "us-central1"
}