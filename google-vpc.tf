resource "google_compute_network" "vpc-octopus" {
  project = var.google_project
  name = var.neomed-vpc-octopus
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "subnet-octopus" {
  project = var.google_project
  name = var.neomed-subnet-octopus
  region = var.region
  network = google_compute_network.vpc-octopus.name
  ip_cidr_range = "10.10.0.0/24"
}
