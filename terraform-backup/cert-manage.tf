# Instalar gerencidor de certificado, no node-pool Tools
resource "helm_release" "cert-manager" {
  name             = var.release-cert-manager 
  namespace        = var.namespace-cert-manager 
  create_namespace = true
  chart            = "cert-manager"
  repository       = var.cert-manager-repo
  version          = var.cert-manager-version 
  depends_on = [
    google_container_cluster.octopus,
    google_container_node_pool.tools,
    kubernetes_namespace.cert-manager
  ]
  set {
    name = "installCRDs"
    value = true
  }
  set {
    name = "webhook.timeoutSeconds"
    value = "4"
  }
  set {
    name = "ingressShim.defaultIssuerName"
    value = "letsencrypt-prod"
  }
  set {
    name = "ingressShim.defaultIssuerKind"
    value = "ClusterIssuer"
  }
  set {
    name = "ingressShim.defaultIssuerGroup"
    value = "cert-manager.io"
  }
  set {
    name = "nodeSelector.cloud\\.google\\.com/gke-nodepool"
    value = "octopus-tools"
  }
  set {
    name = "webhook.nodeSelector.cloud\\.google\\.com/gke-nodepool"
    value = "octopus-tools"
  }
  set {
    name = "cainjector.nodeSelector.cloud\\.google\\.com/gke-nodepool"
    value = "octopus-tools"
  }
 }
#criar cluster issuer para o gerenciador cert-manager
resource "helm_release" "issuer-chart" {
  name = "clusterissuer-chart"
  chart = "./templates/clusterissuer-chart/"
  version = "v1.0.0"
  namespace        = var.namespace-cert-manager
  set {
    name = "letsEncryptClusterIssuer.email"
    value = var.email-issuer
  }
  set {
    name = "addressServer.server"
    value = var.server-letsencrypt
  }
  set {
    name = "privateKeySecretRef.name"
    value = var.private-key-secret-ref
  }
  depends_on = [helm_release.cert-manager]
}

resource "kubernetes_namespace" "cert-manager" {
  metadata {
    name = "cert-manager"
  }
}