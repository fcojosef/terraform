# Instalar o controlador de Ingress  no node pools Tools
resource "helm_release" "ingress-nginx-controller" {
  name       = var.nginx-release-name
  namespace  = var.nginx-ingress-namespace
  repository = var.nginx-ingress-repository
  chart      = "ingress-nginx"
  version    = var.nginx-chart-version
  depends_on = [
    google_container_cluster.octopus,
    google_container_node_pool.tools,
    kubernetes_namespace.nginx-ingress
  ]
  set {
    name  = "controller.replicaCount"
    value = var.number-replicas-nginx-controller
  }
  set {
    name  = "controller.service.externalTrafficPolicy"
    value = "Local"
  }
  set {
    name = "controller.nodeSelector.cloud\\.google\\.com/gke-nodepool"
    value = "octopus-tools"
  }
}
resource "kubernetes_namespace" "nginx-ingress" {
  metadata {
    name = var.nginx-ingress-namespace
  }
}