provider "kubernetes" {
  host                   = "https://${google_container_cluster.octopus.endpoint}"
  cluster_ca_certificate = base64decode(google_container_cluster.octopus.master_auth[0].cluster_ca_certificate)
  token                  = data.google_client_config.provider.access_token
  #load_config_file       = "false"
}

resource "kubernetes_service_account" "gitlab-admin" {
  metadata {
    name      = "gitlab"
    namespace = var.gitlab-namespace
    #namespace = "kube-system"
  }
  depends_on = [
    google_container_cluster.octopus,
    google_container_node_pool.tools,
  ]
}

/* resource "kubernetes_cluster_role_binding" "gitlab-admin" {
  metadata {
    name      = "gitlab-admin"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "gitlab"
    namespace = "kube-system"
  }
  depends_on = [
    kubernetes_cluster_role_binding.gitlab-admin
  ]
} */

resource "kubernetes_namespace" "gitlab-runners" {
  metadata {
    name = var.gitlab-namespace
  }
}



