resource "google_container_cluster" "octopus" {
  project = var.google_project
  name = var.cluster_name
  location = var.region
  min_master_version = var.kubernetes_version
  remove_default_node_pool = true
  initial_node_count = 1

  logging_service    = "logging.googleapis.com/kubernetes" #default option
  monitoring_service = "monitoring.googleapis.com/kubernetes" #default option

  network = google_compute_network.vpc-octopus.name
  subnetwork = google_compute_subnetwork.subnet-octopus.name

  resource_labels = {
    key = "neomed_octopus"
  }
  
  vertical_pod_autoscaling {
    enabled = true
  }
  enable_shielded_nodes = false
  master_auth {
  client_certificate_config {
    issue_client_certificate = true
    }
  }

}
/**
  * Pool de nós (isto é, conjunto de servidores)
  * para todo ambiente do cluster com foco em ferramentas (tools)
*/
resource "google_container_node_pool" "tools" {
  project = var.google_project
  name       = "tools"
  location   = var.region
  cluster    = google_container_cluster.octopus.name
  node_count = var.gke_num_nodes
  version    = var.kubernetes_version
  node_locations = var.zone
  
  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/devstorage.read_only"
    ]
    disk_type = var.disk_type
    disk_size_gb = var.disk_size_gb

    labels = {
      env = var.google_project
    }

    machine_type = var.machine_type
    tags         = ["gke-node-octopus-tools", var.google_project]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}