# Instalar gitlab-runner
resource "helm_release" "gitlab-runner" {
    name       = var.gitlab-release-name
    chart      = var.gitlab-chart
    repository = var.gitlab-repository
    namespace  = var.gitlab-namespace
    version    = var.gitlab-chart-version 
    create_namespace = true
    verify     = true
    timeout    = 3000
    wait_for_jobs = true
    atomic    = true
    cleanup_on_fail = true
    depends_on = [
      google_container_cluster.octopus,
      google_container_node_pool.tools,
      kubernetes_namespace.gitlab-runners
    ]
  set {
    name  = "gitlabUrl"
    value = "https://gitlab.com/"
  }
  set {
    name  = "runnerRegistrationToken"
    value = var.key-gitlab-runner
  }
  set {
    name = "nodeSelector.cloud\\.google\\.com/gke-nodepool"
    value = "octopus-tools"
  }
  set {
    name = "runners.nodeSelector.cloud\\.google\\.com/gke-nodepool"
    value = "octopus-tools"
  }
  set {
    name  = "rbac.create"
    value = "false"
  }
  set {
    name  = "runners.tags"
    value = "runner-octopus"
  }
  set {
    name  = "concurrent"
    value = var.concurrent-runner
  }
  set {
    name  = "checkInterval"
    value = var.check-interval
  }
  set {
    name  = "runners.privileged"
    value = "true"
  }
  set {
    name = "terminationGracePeriodSeconds"
    value = "7200"
  }
  set {
    name = "imagePullPolicy"
    value = "Always"
  }
}

